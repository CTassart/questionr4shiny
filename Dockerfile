FROM rocker/shiny

ENV http_proxy=http://proxy-rie.http.insee.fr:8080
ENV https_proxy=http://proxy-rie.http.insee.fr:8080

# Install debian required packages
RUN echo 'Acquire::http::Proxy "http://proxy-rie.http.insee.fr:8080";' >> /etc/apt/apt.conf && apt-get update -q \
    && DEBIAN_FRONTEND=noninteractive apt-get install -yq --no-install-recommends \
      libssl-dev \
      libgdal-dev \
      libudunits2-dev

# Install R required packages
# ajouter ici les packages à installer
RUN install2.r -e -r https://cran.rstudio.com/ shiny

# Add files in the docker image
ADD ./pgms/app.R /srv/shiny-server/app.R
# ADD ./data/ /srv/shiny-server/data/

