# questionR4Shiny

**Questionnaire d'évaluation sur l'aptitude à suivre une formation, réalisée en shiny**

**WORK IN PROGRESS**

**Squelette de l'application `questionR4Shiny`**
---

Cette application vise a proposer un questionnaire aux candidats à la formation `RShiny`

Au **5 novembre**, elle ne fait qu'afficher 2 (fausses) questions. Il faut donc :

- définir les questions à poser et es implémenter  
    - réaliser les scripts adéquats dans le dossier `questions`,  
- tester et enregistrer la réponse fournie,  
    - éventuellement, enregistrer les résultats,  
    - fournir un visuel pour dire si le résultat est satisafaisant ou non,  
- générer, en `markdown`, un document à transmettre au chargé de la formation.  

Pour le moment, l'applcation, et le projet `gitlab`, sont déployé.e.s sur la [Plateforme innovation](https://questionr4shiny.dev.innovation.insee.eu/). Pour les pérenniser, il faudrait envisager de les déployer ailleurs que via la PFI qui n'assure pas de continuité de service.


